import sqlite3


class BD:
    Nombre = ''
    email = ''
    Carrera = ''
    Ciclo = int
    Cedula = int
    id = int
    def __init__(self):
        self.conexion = sqlite3.connect('academia.sqlite')
        self.cursor = None
        print("Conexion establecida")

    def crear_bd(self):
        sql_academia = """CREATE TABLE academia (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, Cedula INTEGER, 
        Nombre   TEXT UNIQUE, 
        email  TEXT, Carrera TEXTO, Ciclo INTEGER)"""

        self.cursor = self.conexion.cursor()
        self.cursor.execute(sql_academia)
        print("Creadas las tablas")
        self.cursor.close()


    def Matric_est(self,Cedula,Nombre, email, Carrera, Ciclo):
        self.Cedula = Cedula
        self.Nombre = Nombre
        self.email = email
        self.Carrera = Carrera
        self.Ciclo = Ciclo
        self.cursor = self.conexion.cursor()
        self.cursor.executemany("INSERT INTO academia(Cedula, Nombre, email, Carrera, Ciclo) VALUES(?,?,?,?,?)",
                                [(self.Cedula,self.Nombre,self.email,self.Carrera,self.Ciclo)])
        self.conexion.commit()
        self.cursor.close()
        print ("Estudiante matriculado")

    def visualizar_datos(self,Nombre):
        self.Nombre = Nombre
        sql = "SELECT * FROM academia  WHERE Cedula =" + self.Nombre
        self.cursor = self.conexion.cursor()

        filas = self.cursor.execute(sql)
        for f in filas:
            print(f)
        self.cursor.close()

    def buscar_datos(self,id):
        self.id = id
        sql = "SELECT * FROM academia  WHERE id =" + self.id
        self.cursor = self.conexion.cursor()
        filas = self.cursor.execute(sql)
        for f in filas:
            print(f[2])
        self.cursor.close()

    def eliminar_datos(self,id):
        self.id = id
        sql = "DELETE FROM academia  WHERE id =" + self.id
        self.cursor = self.conexion.cursor()
        self.cursor.execute(sql)
        self.conexion.commit()
        self.cursor.close()


if __name__ == "__main__":
    base = BD()
    #base.crear_bd()
    print ('\t\t\tBIENVENIDO \n'
           'Operaciones disponibles en la BD "academia":\n'
           '1.Matricular estudiante.\n'
           '2.Visualizar datos del estudiante\n'
           '3.Buscar estudiante por ID\n'
           '4.Eliminar estudiante por ID\n')
    while True:
        opcion = int(input('¿Qué operación desea realizar? (1,2,3,4) \n'))
        if opcion == 1:
            Cedula = int(input ('Para Matricularse ingrese los siguientes datos\nNúmero de cédula: '))
            Nombre = input ('Nombres completos: ')
            email = input ('Dirección de correo electronico:')
            Carrera = input ('Carrera en la que desea matricularse:')
            Ciclo = int(input('Ciclo al que desea matricularse:'))
            base.Matric_est(Cedula, Nombre, email, Carrera, Ciclo)
        if opcion == 2:
            Cedula = input('Ingrese el número de Cédula a consultar: ')
            base.visualizar_datos(Cedula)
        if opcion == 3:
            id = input('Ingrese un número de id a buscar: ')
            base.buscar_datos(id)
        if opcion == 4:
            id = input('Ingrese un número de id a eliminar: ')
            base.eliminar_datos(id)